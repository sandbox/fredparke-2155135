<?php

/**
 * @file
 * Definition of views_exposed_empty_handler_views_exposed_empty.
 */

/**
 * Views exposed empty custom handler.
 *
 * @ingroup views_area_handlers
 */
class views_exposed_empty_handler_views_exposed_empty extends views_handler_area {

  function option_definition() {
    $options = parent::option_definition();

    $options['view_empty'] = array('default' => '');
    $options['filter_empty'] = array('default' => '');
    return $options;
  }

  /**
   * Default options form that provides the label widget that all fields
   * should have.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['view_empty'] = array(
      '#type' => 'textarea',
      '#title' => t('No results text'),
      '#default_value' => $this->options['view_empty'],
      '#description' => t('Text to display if no results were found.'),
    );

    $form['filter_empty'] = array(
      '#type' => 'textarea',
      '#title' => t('No filter text'),
      '#default_value' => $this->options['filter_empty'],
      '#description' => t('Text to display if no exposed filter was used.'),
    );

  }

  /**
   * Render the area
   */
  function render($empty = FALSE) {
    // If no exposed filter was used.
    if (empty($this->view->exposed_input)) {
      return $this->options['filter_empty'];
    }
    // If no results were found.
    return $this->options['view_empty'];
  }
}
